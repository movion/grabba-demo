package es.movion.grabba.mrz;

public class MRZException extends Exception {
	

	private static final long serialVersionUID = 5309935593274825461L;
	
	public MRZException(String message) {
		super(message);
	}
	

}
