package es.movion.grabba.mrz;

public interface MRZBuilder {
	
	/**
	 * Extrae información de una cadena MRZ que sigue el standard ICAO
	 *  
	 * @param mrz cadena que sigue el standard ICAO
	 * 
	 * @throws MRZException error al leer el formato. Generalmente denota que la cadena de entrada es inválida
	 */
	public void buildFromMRZ(String mrz) throws MRZException ;

}
