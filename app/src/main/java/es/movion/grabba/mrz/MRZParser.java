package es.movion.grabba.mrz;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.movion.utilslib.AppLog;
import es.movion.utilslib.GeneralUtils;


public class MRZParser {

    private static HashMap<String, String> mCountries = new HashMap<>();
    private static final String REGEX_PASSPORT = "^P.*";
    private static final String REGEX_ID = "^(I|A|C).*";

    private static final String LINE_SEPARATOR = "\r?\n|\r";
    private static final char FILLER = '<';
    private static final int[] WEIGHTS = new int[]{7, 3, 1};

    /**
     * Carga el listado de paises soportados
     * <p>
     * NOTA: Este proceso debe hacerse antes de llamar al método {@link MRZParser#getCountryName(String)}
     *
     * @param countries cadena que contiene el listado de países soportados
     */
    public static void loadCountries(String countries) {
        String[] countriesLines = countries.split("\r?\n|\r");
        Pattern pattern = Pattern.compile("([A-Z]{3})\\|(.+)");

        for (int i = 0; i < countriesLines.length; i++) {
            String countryLine = countriesLines[i];
            Matcher matcher = pattern.matcher(countryLine);
            if (matcher.matches()) {
                String countryAbbreviation = matcher.group(1);
                String countryName = matcher.group(2);
                mCountries.put(countryAbbreviation, countryName);
            }
        }
    }

    /**
     * Recupera el nombre asociado a la abreviación.
     * <p>
     * La abreviación debe seguir la ISO 3166-1 alpha-3.
     * <p>
     * NOTA: Es importante haber cargado los países desde fichero antes de intentar hacer una llamada a este método.
     *
     * @param countryAbbreviation abreviación del país
     * @return nombre del país en inglés o null si no se encuentra
     * @see ISO 3166-1 alpha-3 {@link http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3}
     */
    public static String getCountryName(String countryAbbreviation) {
        String result = null;
        if (mCountries != null && countryAbbreviation != null) {
            result = mCountries.get(countryAbbreviation);
        }
        return result;
    }

    /**
     * Comprueba si la cadena mrz es una cádena válida para pasaportes
     *
     * @param mrz cadena a validar
     * @return true si es una cadena válida para un pasaporte
     */
    private static boolean isPassport(String mrz) {
        boolean result = false;
        if (!GeneralUtils.isNullOrEmpty(mrz)) {
            result = clean(mrz).matches(REGEX_PASSPORT);
        }
        return result;
    }

    /**
     * Comprueba si la cadena mrz es una cádena válida para un documento de identidad
     *
     * @param mrz cadena a validar
     * @return true si es una cadena válida para un documento de identidad
     */
    private static boolean isID(String mrz) {
        boolean result = false;
        if (!GeneralUtils.isNullOrEmpty(mrz)) {
            result = clean(mrz).matches(REGEX_ID);
        }
        return result;
    }

    /**
     * Recupera la informazión almacenada en la cadena mrz
     *
     * @param mrz cadena que contiene la información recogida en el documento
     * @return información del documento recuperada
     * @throws MRZException si el el documento no es válido o ocurre un error durante la lectura
     */
    public static MRTD parse(String mrz) throws MRZException {
        MRTD document = null;
        AppLog.d("Parseando...");
        AppLog.d(mrz);
        if (isPassport(mrz)) {
            document = new Passport();
            document.buildFromMRZ(mrz);
        } else if (isID(mrz)) {
            String issuingCountry = mrz.substring(2, 5);
            if ("ESP".equals(issuingCountry)) {
                document = new IDCardSpain();
            } else if ("PRT".equals(issuingCountry)) {
                document = new IDCardPortugal();
            } else {
                document = new IDCard();
            }

            document.buildFromMRZ(mrz);
            AppLog.d("--DOCUMENT VALID---");
        } else {
            throw new MRZException("Document not valid or supported");
        }

        return document;
    }

    /**
     * Elimina los retornos de carro de la cadena de entrada
     *
     * @param input cadena de entrada
     * @return cadena limpiada
     */
    private static String clean(String input) {
        String result = "";
        if (!GeneralUtils.isNullOrEmpty(input))
            result = input.trim().replaceAll(LINE_SEPARATOR, "");
        return result;
    }

    /**
     * Algoritmo de validación para las cadenas leídas
     *
     * @param str        cadena a validar
     * @param checkDigit dígito de control *
     * @return true si la cadena es válida y false en otro caso
     * @throws MRZException si el formato no es válido
     * @see {@link http://www.highprogrammer.com/alan/numbers/mrp.html#checkdigit}
     */
    public static boolean isValid(String str, int checkDigit) throws MRZException {
        int result = 0;
        for (int i = 0; i < str.length(); i++) {
            result += getCharacterValue(str.charAt(i)) * WEIGHTS[i % WEIGHTS.length];
        }
        return result % 10 == checkDigit;
    }

    /**
     * Devuelve el valor asignado a un carácter para la validación. Cada carácter tiene un número asignado que se usa
     * para el proceso de validación
     *
     * @param c carácter a evaluar
     * @return el valor entero del caráter
     * @throws MRZException si el carácter no está soportado
     * @see {@link http://www.highprogrammer.com/alan/numbers/mrp.html#checkdigit}
     */
    public static int getCharacterValue(char c) throws MRZException {
        if (c == FILLER) {
            return 0;
        }
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'Z') {
            return c - 'A' + 10;
        }
        throw new MRZException("Invalid character in MRZ record: " + c);
    }

    /**
     * Comprueba si el elemento es el carácter separador o no
     *
     * @param character elemento a evaluar
     * @return true si es un carácter separador y false en otro caso
     */
    public static boolean isFiller(String character) {
        return String.valueOf(FILLER).equals(character);
    }

    /**
     * Limpia la cadena de entrada de separadores
     *
     * @param str cadena a limpiar
     * @return cadena limpia
     */
    public static String removeFillers(String str) {
        String result = "";
        if (!GeneralUtils.isNullOrEmpty(str)) {
            result = str.replaceAll(String.valueOf(FILLER), "").trim();
        }

        return result;
    }

    /**
     * Limpia la cadena de entrada de separadores
     *
     * @param str cadena a limpiar
     * @return cadena limpia
     */
    public static String replaceFillers(String str, String replace) {
        String result = "";
        if (!GeneralUtils.isNullOrEmpty(str) && replace != null) {
            str.replaceAll(String.valueOf(FILLER), replace);
        }
        return result;
    }

    /**
     * Parsea una línea que contiene información relativa al nombre
     * <p>
     * NOTA: la línea debe seguir el formato "APELLIDOS"<<"NOMBRE" donde tanto "APELLIDOS" como "NOMBRE" pueden tener
     * separadores.
     *
     * @param line línea a parsear
     * @return un array de dos cadenas donde la primera representa los apellidos y la segunda el nombre
     */
    public static String[] parseName(String line) {
        String[] result = {"", ""};
        if (!GeneralUtils.isNullOrEmpty(line)) {
            String[] fullName = line.split("<<");
            String lastName = fullName[0].replace(FILLER, ' ').trim();
            String firstName = fullName[1].replace(FILLER, ' ').trim();
            result[0] = lastName;
            result[1] = firstName;
        }
        return result;
    }

    /**
     * Dada una cadena de entrada la separa en diferentes líneas usando el salto de línea como separador
     *
     * @param str cadena mrz
     * @return array que contiene cada una de la líneas separadas
     */
    public static String[] splitLines(String str) {
        return str.split(LINE_SEPARATOR);
    }

}
