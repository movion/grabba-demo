package es.movion.grabba.mrz;

public class IDCardSpain extends IDCard {

	@Override
	protected String getDocumentNumber(String line) throws MRZException {
		String result = null;
		
		String documentNumber = line.substring(5, 14);
		int documentNumberCheckDigit = Integer.parseInt(line.substring(14, 15));
		if (!MRZParser.isValid(documentNumber, documentNumberCheckDigit)) {
			throw new MRZException("Error, validation fails");
		} else {
			//En el DNI nuevo, el número se encuentra en el campo opcional
			String optional = MRZParser.removeFillers(line.substring(15));
			if (optional.length() > 0) {
				result = optional;
			} else {
				result = documentNumber;
			}
		}
		
		return result;
	}

}
