package es.movion.grabba.mrz;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IDCardPortugal extends IDCard {

	@Override
	protected String getDocumentNumber(String line) throws MRZException {
		String result = null;
		String number = line.substring(5, 13);
		String numberCheck = line.substring(13,14);
		String version = line.substring(15,17);
		String versionCheck = line.substring(17,18);
		String numberCC = number + numberCheck + version + versionCheck;
		String fullNumber = line.substring(5,18);
		int fullNumberCheckDigit = Integer.parseInt(line.substring(18,19));
		
		if (MRZParser.isValid(fullNumber, fullNumberCheckDigit) && isValidCC(numberCC)) {
			result = number + " " + numberCheck + " " + version + versionCheck;
		} else {
			throw new MRZException("Validation fails");
		}

		return result;
	}
	
	public boolean isValidCC(String str) throws MRZException {		
		int result = 0;
		Pattern pattern = Pattern.compile("[0-9]{9}[A-Z]{2}[0-9]");
		Matcher matcher = pattern.matcher(str);
		if (matcher.matches()) {
			for (int i = 0; i < str.length(); i++) {
				int value = MRZParser.getCharacterValue(str.charAt(i));
				if (i % 2 == 0) {
					value *= 2;
					if (value > 9) 
						value-= 9;
				}
				result += value;				
			}
		} else {
			throw new MRZException("Invalid number format");
		}
		
		return result % 10 == 0;
		
	}

}
