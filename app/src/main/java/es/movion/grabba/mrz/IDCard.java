package es.movion.grabba.mrz;

import android.os.Parcel;

public class IDCard extends MRTD {

	public IDCard() {
		initValues(TYPE_ID);
	}

	private IDCard(Parcel in) {
		initValues(TYPE_ID);
		readFromParcel(in);
	}

	public void buildFromMRZ(String mrz) throws MRZException {
		try {
			String[] lines = MRZParser.splitLines(mrz);
			if (lines.length == 3) {
				String line1 = lines[0];
				String line2 = lines[1];
				String line3 = lines[2];
				if (line1 != null && line2 != null && line3 != null && line1.length() == 30 && line2.length() == 30
						&& line3.length() == 30) {

					// País emisor					
					String issuingCountry = line1.substring(2, 5);

					// Document Number
					String documentNumber = getDocumentNumber(line1);

					// Name
					String[] fullName = MRZParser.parseName(line3);
					String lastName = fullName[0];
					String firstName = fullName[1];

					// Fecha de nacimiento
					String dateOfBirth = line2.substring(0, 6);
					int dateOfBirthCheckDigit = Integer.parseInt(line2.substring(6, 7));

					// Sexo
					String sex = line2.substring(7, 8);
					if (!"M".equals(sex) && !"F".equals(sex)) {
						sex = "-";
					}

					// Fecha de caducidad 						
					String expirationDate = line2.substring(8, 14);
					int expirationDateCheckDigit = Integer.parseInt(line2.substring(14, 15));

					// Nacionalidad
					String nationality = MRZParser.getCountryName(line2.substring(15, 18));
					if (nationality == null) {
						nationality = "-";
					}

					// Validación sobre los digitos  6–30 (línea1) y 1–7, 9–15, 19–29 (línea 2)
					String fullLineValidation = line1.substring(5, 30) + line2.substring(0, 7) + line2.substring(8, 15)
							+ line2.substring(18, 29);
					int fullLineCheckDigit = Integer.parseInt(line2.substring(29, 30));

					if (MRZParser.isValid(dateOfBirth, dateOfBirthCheckDigit)
							&& MRZParser.isValid(expirationDate, expirationDateCheckDigit)
							&& MRZParser.isValid(fullLineValidation, fullLineCheckDigit)
                            ){
						String documentNumberClean = MRZParser.removeFillers(documentNumber);

						setNumber(documentNumberClean);
						setFirstName(firstName);
						setLastName(lastName);
						setDateOfBirth(dateOfBirth);
						setExpirationDate(expirationDate);
						setSex(sex);
						setIssuingCountry(issuingCountry);
						setNationality(nationality);

					} else {
						throw new MRZException("Error, validation fails");
					}

				} else {
					throw new MRZException("Error, invalid passport format");
				}

			} else {
				throw new MRZException("Error, invalid passport format");
			}

		} catch (Exception e) {
			throw new MRZException(e.getMessage());
		}
	}

	protected String getDocumentNumber(String line) throws MRZException {

		String result = null;

		String documentNumber = line.substring(5, 14);
		int documentNumberCheckDigit = Integer.parseInt(line.substring(14, 15));
		if (!MRZParser.isValid(documentNumber, documentNumberCheckDigit)) {
			throw new MRZException("Error, validation fails");
		} else {
			result = documentNumber;
		}

		return result;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getIssuingCountry());
		dest.writeString(getNationality());
		dest.writeString(getFirstName());
		dest.writeString(getLastName());
		dest.writeString(getNumber());
		dest.writeString(getExpirationDate());
		dest.writeString(getDateOfBirth());
		dest.writeString(getSex());
		dest.writeInt(getType());
	}

	/*
	 * Creador que usa la interfaz Parcelable para serializar y des-serializar los objetos Parcel
	 */
	public static final Creator<IDCard> CREATOR = new Creator<IDCard>() {
		public IDCard createFromParcel(Parcel in) {
			return new IDCard(in);
		}

		public IDCard[] newArray(int size) {
			return new IDCard[size];
		}
	};

}
