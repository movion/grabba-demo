package es.movion.grabba.mrz;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Modelo de datos que contiene información relativa a un pasaporte
 * 
 * @author hrahal
 * 
 */
public class Passport extends MRTD {

	public Passport() {
		initValues(TYPE_PASSPORT);
	}

	private Passport(Parcel in) {
		initValues(TYPE_PASSPORT);
		readFromParcel(in);
	}

	/**
	 * Parsea la información de un pasaporte contenida en una cadena mrz que standard el formato ICAO
	 * 
	 * @see {@link http://en.wikipedia.org/wiki/Machine-readable_passport}
	 * @param mrz cadena que sigue el standard ICAO para pasaportes
	 * @throws MRZException error al leer el formato. Generalmente denota que la cadena de entrada es inválida
	 */
	@Override
	public void buildFromMRZ(String mrz) throws MRZException {

		try {
			String[] lines = MRZParser.splitLines(mrz);
			if (lines.length == 2) {
				String line1 = lines[0];
				String line2 = lines[1];
				if (line1 != null && line2 != null && line1.length() == 44 && line2.length() == 44) {

					// País emisor					
					String issuingCountry = line1.substring(2, 5);

					// Nombre
					String[] fullName = MRZParser.parseName(line1.substring(5));
					String lastName = fullName[0];
					String firstName = fullName[1];

					// Número de pasaporte
					String passportNumber = line2.substring(0, 9);
					int passportNumberCheckDigit = Integer.parseInt(line2.substring(9, 10));

					// Nacionalidad
					String nationality = MRZParser.getCountryName(line2.substring(10, 13));
					if (nationality == null) {
						nationality = "-";
					}

					// Fecha de nacimiento
					String dateOfBirth = line2.substring(13, 19);
					int dateOfBirthCheckDigit = Integer.parseInt(line2.substring(19, 20));

					// Sexo
					String sex = line2.substring(20, 21);
					if (!"M".equals(sex) && !"F".equals(sex)) {
						sex = "-";
					}

					// Fecha de caducidad		
					String expirationDate = line2.substring(21, 27);
					int expirationDateCheckDigit = Integer.parseInt(line2.substring(27, 28));

					// Personal number
					String personalNumber = line2.substring(28, 42);
					String personalNumberCheck = line2.substring(42, 43);
					int personalNumberCheckDigit = 0;

					// Si el carácter de control no es un separador debe de ser un dígito
					if (!MRZParser.isFiller(personalNumberCheck)) {
						personalNumberCheckDigit = Integer.parseInt(personalNumberCheck);
					}

					// Validación sobre los dígitos 1–10, 14–20, and 22–43
					String fullLineValidation = line2.substring(0, 10) + line2.substring(13, 20)
							+ line2.substring(21, 43);
					int fullLineCheckDigit = Integer.parseInt(line2.substring(43, 44));

					if (MRZParser.isValid(passportNumber, passportNumberCheckDigit)
							&& MRZParser.isValid(dateOfBirth, dateOfBirthCheckDigit)
							&& MRZParser.isValid(expirationDate, expirationDateCheckDigit)
							&& MRZParser.isValid(personalNumber, personalNumberCheckDigit)
							&& MRZParser.isValid(fullLineValidation, fullLineCheckDigit)) {
						String passportNumberClean = MRZParser.removeFillers(passportNumber);
						setNumber(passportNumberClean);
						setFirstName(firstName);
						setLastName(lastName);
						setDateOfBirth(dateOfBirth);
						setExpirationDate(expirationDate);
						setIssuingCountry(issuingCountry);
						setNationality(nationality);
						setSex(sex);
					} else {
						throw new MRZException("Error, validation fails");
					}

				} else {
					throw new MRZException("Error, invalid passport format");
				}
			} else {
				throw new MRZException("Error, invalid passport format");
			}

		} catch (Exception e) {
			throw new MRZException(e.getMessage());
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getIssuingCountry());
		dest.writeString(getNationality());
		dest.writeString(getFirstName());
		dest.writeString(getLastName());
		dest.writeString(getNumber());
		dest.writeString(getExpirationDate());
		dest.writeString(getDateOfBirth());
		dest.writeString(getSex());
		dest.writeInt(getType());
	}

	/*
	 * Creador que usa la interfaz Parcelable para serializar y des-serializar los objetos Parcel
	 */
	public static final Parcelable.Creator<Passport> CREATOR = new Parcelable.Creator<Passport>() {
		public Passport createFromParcel(Parcel in) {
			return new Passport(in);
		}

		public Passport[] newArray(int size) {
			return new Passport[size];
		}
	};
}
