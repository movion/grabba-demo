package es.movion.grabba.mrz;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * MRTD: machine-readable travel document 
 * 
 * @author hrahal
 *
 */
public abstract class MRTD implements MRZBuilder, Parcelable {
	
	public static final int TYPE_UNDEFINED = 0;
	public static final int TYPE_PASSPORT = 1;
	public static final int TYPE_ID = 2;

	private String issuingCountry;
	private String nationality;
	private String firstName;
	private String lastName;
	private String number;
	private String expirationDate;
	private String dateOfBirth;
	private String sex;
	private int type;
	
	/**
	 * Inicia los valores por defecto para un tipo definido
	 * 
	 * @param type tipo a usar
	 */
	protected void initValues(int type) {
		setIssuingCountry("");
		setNationality("");
		setFirstName("");
		setLastName("");
		setNumber("");
		setExpirationDate("");
		setDateOfBirth("");
		setSex("");
		setType(type);
	}
	
	/**
	 * Llamado por el constructor para crear este objecto de un un parcel 
	 * 
	 * @param in parcel de la cual sacamo información para re-crear el objeto
	 */
	public void readFromParcel(Parcel in) {
		setIssuingCountry(in.readString());
		setNationality(in.readString());
		setFirstName(in.readString());
		setLastName(in.readString());
		setNumber(in.readString());
		setExpirationDate(in.readString());
		setDateOfBirth(in.readString());
		setSex(in.readString());
		setType(in.readInt());
	}
	
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the expirationDate
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the issuingCountry
	 */
	public String getIssuingCountry() {
		return issuingCountry;
	}

	/**
	 * @param issuingCountry the issuingCountry to set
	 */
	public void setIssuingCountry(String issuingCountry) {
		this.issuingCountry = issuingCountry;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
}
