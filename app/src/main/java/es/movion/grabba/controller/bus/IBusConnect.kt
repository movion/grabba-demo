package es.movion.grabba.controller.bus

/**
 * Interfaz de conexión/desconexión de eventos de un objeto
 */
interface IBusConnect {
    /**
     * Conecta una instancia con los eventos del objeto
     *
     * @param object
     */
    fun connect(`object`: Any)

    /**
     * Desconecta una instancia con los eventos del objeto
     *
     * @param object
     */
    fun disconnect(`object`: Any)

    /**
     * Emite el evento pasado como parámetro
     *
     * @param event
     */
    fun emit(event: Any)
}