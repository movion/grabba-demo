package es.movion.grabba.controller.grabba

import android.os.AsyncTask
import com.grabba.*
import com.grabba.preferences.GrabbaBarcodePreferences
import com.grabba.preferences.GrabbaPreferences
import es.movion.grabba.GrabbaBarcodeApp
import es.movion.grabba.R
import es.movion.grabba.controller.bus.BusController
import es.movion.grabba.controller.grabba.event.*

/**
 * Created by Victor on 17/10/2017.
 */

class GrabbaController private constructor()//singleton
    : BusController(), GrabbaConnectionListener {

    var scanning = false

    fun initDevice() {
        try {
            Grabba.open(GrabbaBarcodeApp.getAppContext(), GrabbaBarcodeApp.getAppContext().getString(R.string.app_name))
            emit(GrabbaInitedEvent())
        } catch (e: GrabbaDriverNotInstalledException) {
            emit(GrabbaInitedEvent(e))
        } catch (e: GrabbaNotOpenException) {
            emit(GrabbaInitedEvent(e))
        }

    }

    fun adquireAll(barcodeListener: GrabbaBarcodeListener, buttonListener: GrabbaButtonListener, passportListener: GrabbaPassportListener, magstripeListener: GrabbaMagstripeListener) {
        try {
            Grabba.getInstance().addConnectionListener(this)
            Grabba.getInstance().addButtonListener(buttonListener)
            GrabbaPassport.getInstance().addEventListener(passportListener)
            GrabbaBarcode.getInstance().addEventListener(barcodeListener)
            GrabbaMagstripe.getInstance().addEventListener(magstripeListener)
            SetGrabbaPreferencesTask().execute()
            Grabba.getInstance().acquireGrabba()
            CheckGrabbaConnectionTask().execute()
        } catch (exception: GrabbaNotOpenException) {
            emit(GrabbaConnectionEvent(false))
        }
    }

    /**
     * Realiza un escaneo de barcode
     * No llamar en el hilo principal
     */
    fun scanBarcode() {
        try {
            showBatteryLevel()
            GrabbaBarcode.getInstance().trigger(true)
        } catch (e: GrabbaNotConnectedException) {
            e.printStackTrace()
        } catch (e: GrabbaBusyException) {
            e.printStackTrace()
        } catch (e: GrabbaNoExclusiveAccessException) {
            e.printStackTrace()
        } catch (e: GrabbaEventDispatchException) {
            e.printStackTrace()
        } catch (e: GrabbaFunctionNotSupportedException) {
            e.printStackTrace()
        }

    }

    /**
     * Realiza un escaneo de barcode
     * No llamar en el hilo principal
     */
    fun stopBarcode() {
        try {
            GrabbaBarcode.getInstance().trigger(false)
        } catch (e: GrabbaNotConnectedException) {
            e.printStackTrace()
        } catch (e: GrabbaBusyException) {
            e.printStackTrace()
        } catch (e: GrabbaNoExclusiveAccessException) {
            e.printStackTrace()
        } catch (e: GrabbaEventDispatchException) {
            e.printStackTrace()
        } catch (e: GrabbaFunctionNotSupportedException) {
            e.printStackTrace()
        }
    }

    fun continuousBarcodeScan() {
        scanning = !scanning
        ContinuousScanTask().execute(scanning)
    }

    override fun grabbaConnectedEvent() {
        showBatteryLevel()
        emit(GrabbaConnectionEvent(true))
    }

    fun showBatteryLevel() {
        try {
            emit(GrabbaBatteryLevelEvent(Grabba.getInstance().batteryLevel.toFloat()))
        } catch (e: GrabbaNotConnectedException) {
            e.printStackTrace()
        } catch (e: GrabbaIOException) {
            e.printStackTrace()
        } catch (e: GrabbaBusyException) {
            e.printStackTrace()
        } catch (e: GrabbaNoExclusiveAccessException) {
            e.printStackTrace()
        } catch (e: GrabbaEventDispatchException) {
            e.printStackTrace()
        }

    }

    override fun grabbaDisconnectedEvent() {
        emit(GrabbaConnectionEvent(false))
    }

    fun releaseAll(barcodeListener: GrabbaBarcodeListener, buttonListener: GrabbaButtonListener, passportListener: GrabbaPassportListener, magstripeListener: GrabbaMagstripeListener) {
        try {
            Grabba.getInstance().removeConnectionListener(this)
            Grabba.getInstance().removeButtonListener(buttonListener)
            GrabbaPassport.getInstance().removeEventListener(passportListener)
            GrabbaBarcode.getInstance().removeEventListener(barcodeListener)
            GrabbaMagstripe.getInstance().removeEventListener(magstripeListener)
            Grabba.getInstance().releaseGrabba()
            CheckGrabbaConnectionTask().execute()
        } catch (exception: GrabbaNotOpenException) {
            emit(GrabbaConnectionEvent(false))
        }

    }

    private inner class CheckGrabbaConnectionTask : AsyncTask<Void, Void, Boolean>() {
        override fun doInBackground(vararg params: Void): Boolean? {
            var connected = java.lang.Boolean.valueOf(false)
            try {
                // Esta llamada debe realizarse en segundo plano
                connected = java.lang.Boolean.valueOf(Grabba.getInstance().isConnected)
            } catch (e: GrabbaNoExclusiveAccessException) {
                //AppLog.e("Grabba not acquired", e)
            }

            return connected
        }

        override fun onPostExecute(result: Boolean?) {
            emit(GrabbaConnectionEvent(result != null && result))
        }
    }

    private inner class SetGrabbaPreferencesTask : AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void?): Boolean {
            Grabba.getInstance().setBoolPreference(GrabbaBarcodePreferences.Symbology.UPCA.UPCAtoEAN13ReformatBool, true)
            Grabba.getInstance().setBoolPreference(GrabbaPreferences.extendedBatteryWarningBool, false)
            Grabba.getInstance().setIntPreference(GrabbaPreferences.extendedBatteryWarningThresholdInt, 30)
            Grabba.getInstance().savePreferences()
            return Grabba.getInstance().getBoolPreference(GrabbaBarcodePreferences.Symbology.UPCA.UPCAtoEAN13ReformatBool)
        }

        override fun onPostExecute(result: Boolean?) {
            emit(GrabbaPreferencesUpdatedEvent(result != null && result))
        }

    }

    private inner class ContinuousScanTask : AsyncTask<Boolean, Void, String>() {

        override fun doInBackground(vararg params: Boolean?): String {
            var init = params[0] == true
            try {
                GrabbaBarcode.getInstance().presentationMode(init)
            } catch (e: GrabbaFunctionNotSupportedException) {
                e.printStackTrace()
                return "Operación no soportada"
            } catch (e: GrabbaNotConnectedException) {
                e.printStackTrace()
                return "Dispositivo no conectado"
            }
            if (scanning)
                return "Escaneo iniciado"
            else
                return "Escaneo finalizado"
        }

        override fun onPostExecute(result: String?) {
            var message = ""
            if (!result.isNullOrBlank())
                message = result!!
            emit(GrabbaStatusUpdatedEvent(message))
        }
    }

    companion object {
        val instance = GrabbaController()
    }
}
