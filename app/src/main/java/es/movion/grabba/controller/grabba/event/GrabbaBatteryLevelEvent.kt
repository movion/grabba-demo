package es.movion.grabba.controller.grabba.event

class GrabbaBatteryLevelEvent(batteryLevel: Float) {

    val batteryLevel: Int = batteryLevel.toInt()

}
