package es.movion.grabba.controller.grabba.event

class GrabbaPreferencesUpdatedEvent(val isUpdated: Boolean)
