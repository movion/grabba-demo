package es.movion.grabba.controller.grabba.event

class GrabbaConnectionEvent(val isConnected: Boolean)
