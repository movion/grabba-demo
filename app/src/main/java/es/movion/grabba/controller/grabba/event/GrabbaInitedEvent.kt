package es.movion.grabba.controller.grabba.event

class GrabbaInitedEvent {

    var exception: Exception? = null

    val isSuccess: Boolean
        get() = exception == null

    constructor()

    constructor(e: Exception) {
        this.exception = e
    }
}
