package es.movion.grabba.controller.bus

import org.greenrobot.eventbus.EventBus

/**
 * Controlador de lanzamiento de eventos por EventBus
 * <p>
 * Nota: Por defecto no lanza los eventos como sticky, redefinir método {@link #launchEventAsSticky(Object)}
 * para darle un comportamiento diferente
 */
abstract class BusController : IBusConnect {

    private var bus: EventBus = EventBus.builder().build()

    override fun connect(`object`: Any) {
        bus.register(`object`)
    }

    override fun disconnect(`object`: Any) {
        bus.unregister(`object`)
    }

    override fun emit(event: Any) {
        if (launchEventAsSticky(event))
            bus.postSticky(event)
        else
            bus.post(event)
    }

    protected fun launchEventAsSticky(event: Any): Boolean {
        return false
    }
}