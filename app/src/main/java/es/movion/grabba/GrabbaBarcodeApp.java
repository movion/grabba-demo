package es.movion.grabba;

import android.app.Application;
import android.content.Context;

import es.movion.utilslib.AppLog;

public class GrabbaBarcodeApp extends Application{

    private static final String LOG_TAG_APP_NAME = "GrabbaBarcodeApp";
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        GrabbaBarcodeApp.context = getApplicationContext();
        setupAppLog();
    }

    private void setupAppLog() {
        AppLog.setAppName(LOG_TAG_APP_NAME);
        AppLog.setLogLevel(AppLog.LEVEL_DEBUG);
    }

    public static Context getAppContext() {
        return context;
    }
}
