package es.movion.grabba.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.View
import android.widget.Toast
import com.grabba.GrabbaBarcodeListener
import com.grabba.GrabbaButtonListener
import com.grabba.GrabbaMagstripeListener
import com.grabba.GrabbaPassportListener
import es.movion.grabba.R
import es.movion.grabba.controller.grabba.GrabbaController
import es.movion.grabba.controller.grabba.event.*
import es.movion.grabba.databinding.ActivityMainBinding
import es.movion.grabba.mrz.MRTD
import es.movion.grabba.mrz.MRZException
import es.movion.grabba.mrz.MRZParser
import es.movion.utilslib.AppLog
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : AppCompatActivity(), GrabbaBarcodeListener, GrabbaButtonListener, GrabbaPassportListener, GrabbaMagstripeListener {

    lateinit var binding: ActivityMainBinding
    lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding.toolbar)
    }

    override fun onStart() {
        super.onStart()
        GrabbaController.instance.connect(this)
        GrabbaController.instance.initDevice()
        GrabbaController.instance.adquireAll(this, this, this, this)
    }

    override fun onStop() {
        super.onStop()
        GrabbaController.instance.releaseAll(this, this, this, this)
        GrabbaController.instance.disconnect(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGrabbaInited(event: GrabbaInitedEvent) {
        if (!event.isSuccess) {
            Toast.makeText(this, "Controlador no instalado", Toast.LENGTH_SHORT).show()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGrabbaConnection(event: GrabbaConnectionEvent) {
        val message = if (event.isConnected) "Graba conectado" else "Grabba no encontrado"
        var drawableRes = if (event.isConnected) R.drawable.ic_action_connected else R.drawable.ic_action_disconnected
        menu.findItem(R.id.connection)?.setIcon(drawableRes)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        if (!event.isConnected)
            binding.battery.text = "-%"
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGrabbaPreferencesUpdated(event: GrabbaPreferencesUpdatedEvent) {
        val message = if (event.isUpdated) "Preferencias actualizadas" else "Preferencias no actualizadas"
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGrabbaBatteryLevel(event: GrabbaBatteryLevelEvent) {
        binding.battery.text = event.batteryLevel.toString() + "%"
    }

    fun onClickContinuousScan(view: View) {
        GrabbaController.instance.continuousBarcodeScan()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGrabbaStatusUpdated(event: GrabbaStatusUpdatedEvent) {
        Toast.makeText(this, event.message, Toast.LENGTH_SHORT).show()
    }


    override fun passportReadEvent(mrz: String?) {
        GrabbaController.instance.showBatteryLevel()
        var document: MRTD? = null
        try {
            document = MRZParser.parse(mrz)
        } catch (e: MRZException) {
            document = null
            Toast.makeText(this, R.string.error_mrz_not_valid, Toast.LENGTH_SHORT).show()
        } finally {
            if (document != null) {
                showDocumentInfo(document)
            }
        }
    }

    private fun showDocumentInfo(document: MRTD) {
        showDataOnUi(document.number + "\n" + document.firstName + " " + document.lastName)
    }

    override fun barcodeScannedEvent(p0: String?, p1: Int) {
        var original = p0!!
        GrabbaController.instance.stopBarcode()
        if (Looper.myLooper() == Looper.getMainLooper())
            binding.textview.text = original
        else
            Handler(Looper.getMainLooper()).post {
                binding.textview.text = original
            }
    }

    override fun barcodeTriggeredEvent() {
        AppLog.e("Escaneo iniciado")
    }

    override fun barcodeScanningStopped() {
        AppLog.e("Escaneo finalizado")
    }

    override fun barcodeTimeoutEvent() {
        GrabbaController.instance.stopBarcode()
    }

    override fun grabbaRightButtonEvent(p0: Boolean) {
        GrabbaController.instance.scanBarcode()
    }

    override fun grabbaLeftButtonEvent(p0: Boolean) {
        GrabbaController.instance.scanBarcode()
    }

    override fun magstripeRawReadEvent(p0: ByteArray?, p1: ByteArray?, p2: ByteArray?) {
        showDataOnUi(
                "p0: " + returnString(p0) +
                        "\np1: " + returnString(p1) +
                        "\np2: " + returnString(p2))
    }

    override fun magstripeReadEvent(p0: ByteArray?, p1: ByteArray?, p2: ByteArray?) {
        showDataOnUi(
                "p0: " + returnString(p0) +
                        "\n\np1: " + returnString(p1) +
                        "\n\np2: " + returnString(p2)
        )
    }

    fun returnString(byteArray: ByteArray?): String {
        return if (byteArray != null) String(byteArray) else ""
    }

    fun showDataOnUi(message: String) {
        runOnUiThread {
            binding.textview.text = message
        }
    }
}
